package com.equalexperts.test.specification


import com.equalexperts.test.annotations.Resource
import com.github.fge.jsonschema.main.JsonSchemaFactory
import org.glassfish.hk2.api.ServiceLocator
import org.glassfish.hk2.utilities.ServiceLocatorUtilities
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.glassfish.jersey.server.ResourceConfig
import org.glassfish.jersey.test.JerseyTest
import org.jongo.Jongo
import org.jongo.MongoCollection
import spock.lang.Shared
import spock.lang.Specification

import javax.ws.rs.client.WebTarget

abstract class ResourceSpecification extends Specification {
    @Shared
    Jongo jongo

    @Shared
    ServiceLocator locator

    @Shared
    JsonSchemaFactory schemaFactory

    @Shared
    Class<?> resourceClass

    @Shared
    String collectionName

    @Shared
    String providerPkgs

    MongoCollection collection
    JerseyTest test

    def setupSpec() {
        schemaFactory = JsonSchemaFactory.byDefault()
        locator = ServiceLocatorUtilities.bind(getBinder())

        jongo = locator.getService(Jongo)

        if (!getClass().isAnnotationPresent(Resource)) {
            throw new Exception("No Resource annotation found")
        }

        def resourceAnnotation = getClass().getAnnotation(Resource)
        resourceClass = resourceAnnotation.type()
        providerPkgs = resourceAnnotation.providerPkgs()
        collectionName = resourceAnnotation.collection()
    }

    def setup() {
        cleanupDb()

        def config = new ResourceConfig(resourceClass)
        config.register(binder)
        config.packages(true, providerPkgs)

        test = new JerseyTest(config) {}
        test.setUp()
    }

    def cleanup() {
        test?.tearDown()
    }
    private void cleanupDb() {
        if (collectionName) {
            collection = jongo.getCollection(collectionName)
            collection.remove()
        }
    }

    public WebTarget getTarget() {
        test.target()
    }

    abstract AbstractBinder getBinder()
}
