package com.equalexperts.products.service.providers

import com.equalexperts.products.domain.Product
import com.equalexperts.products.service.MediaTypes
import com.fasterxml.jackson.databind.ObjectMapper
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap
import spock.lang.Specification

import javax.ws.rs.Produces
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import java.lang.annotation.Annotation

class ProductListV2MessageBodyWriterSpec extends Specification {
    private ObjectMapper objectMapper

    def setup() {
        objectMapper = new ObjectMapper()
    }

    def "should produce the correct media types"() {
        expect:
        def annotation = ProductListV2MessageBodyWriter.getAnnotation(Produces)

        annotation != null

        def value = annotation.value()
        value.size() == 1

        value.contains(MediaTypes.PRODUCT_LIST_V2_JSON)
    }

    def "should indicate it is the writer for products"() {
        expect:
        def writer = new ProductListV2MessageBodyWriter(objectMapper)

        writer.isWriteable(clazz, genericType, new Annotation[0], new MediaType()) == result

        where:
        clazz  | genericType                              | result
        List   | new GenericType<List<Product>>() {}.type | true
        String | new GenericType<List<String>>() {}.type  | false
    }

    def "should return the correct representation"() {
        given:
        def product1 = new Product(productId: "product1", description: "Product 1", price: 19999, color: "red")
        def product2 = new Product(productId: "product2", description: "Product 2", price: 19999, color: "blue")

        def writer = new ProductListV2MessageBodyWriter(objectMapper)

        def outputStream = new ByteArrayOutputStream()

        when:
        writer.writeTo([product1, product2], Product, null, new Annotation[0], new MediaType(), new MultivaluedStringMap(), outputStream)

        then:
        def type = objectMapper.getTypeFactory().constructCollectionType(List, Map)

        def products = objectMapper.readValue(outputStream.toByteArray(), type)
        products.size == 2

        def product = products.first()
        product.productId == product.productId
        product.description == product.description
        product.price == "199.99"
        product.color == product.color
    }
}
