package com.equalexperts.products.service.resources

import com.equalexperts.common.config.MongoConfiguration
import com.equalexperts.products.DependencyBinder
import com.equalexperts.products.domain.Product
import com.equalexperts.products.service.MediaTypes
import com.equalexperts.test.annotations.Resource
import com.equalexperts.test.specification.ResourceSpecification
import com.github.fge.jackson.JsonLoader
import org.glassfish.hk2.utilities.binding.AbstractBinder
import spock.lang.Unroll

import javax.ws.rs.core.MediaType

import static javax.ws.rs.core.Response.Status.NOT_FOUND
import static javax.ws.rs.core.Response.Status.OK

@Resource(
        type = ProductResource,
        collection = "products",
        providerPkgs = "com.equalexperts.products.service.providers"
)
class ProductResourceSpec extends ResourceSpecification {
    Product product1
    Product product2

    def setup() {
        product1 = new Product(productId: "product1", description: "A product", price: 19999, color: "red")
        product2 = new Product(productId: "product2", description: "A product", price: 19999, color: "red")

        collection.insert(product1, product2)
    }

    @Override
    AbstractBinder getBinder() {
        new DependencyBinder(new MongoConfiguration(host: "app-monitor", port: 27017, db: "products", user: "product-user", password: "s3cr3t"))
    }

    def "should return a correctly formatted list response"() {
        given:
        def request = target.path("product/list").request(mediaType)

        when:
        def response = request.get()

        then:
        response.status == OK.statusCode

        def entity = response.readEntity(String)
        def schema = schemaFactory.getJsonSchema(JsonLoader.fromResource(schemaPath))
        def result = schema.validate(JsonLoader.fromString(entity))

        result.success

        where:
        mediaType                       | schemaPath
        MediaType.APPLICATION_JSON      | "/schemas/product-list-v1-schema.json"
        MediaTypes.PRODUCT_LIST_V1_JSON | "/schemas/product-list-v1-schema.json"
        MediaTypes.PRODUCT_LIST_V2_JSON | "/schemas/product-list-v2-schema.json"
    }

    def "should return a correctly formatted detail response"() {
        given:
        def request = target.path("product/$product1.productId").request(mediaType)

        when:
        def response = request.get()

        then:
        response.status == OK.statusCode

        def entity = response.readEntity(String)
        def schema = schemaFactory.getJsonSchema(JsonLoader.fromResource(schemaPath))
        def result = schema.validate(JsonLoader.fromString(entity))

        result.success

        where:
        mediaType                  | schemaPath
        MediaType.APPLICATION_JSON | "/schemas/product-v1-schema.json"
        MediaTypes.PRODUCT_V1_JSON | "/schemas/product-v1-schema.json"
        MediaTypes.PRODUCT_V2_JSON | "/schemas/product-v2-schema.json"
    }

    def "should return an error if request is made for the details of a non existent product"() {
        given:
        def request = target.path("product/unknown").request(MediaType.APPLICATION_JSON)

        when:
        def response = request.get()

        then:
        response.status == NOT_FOUND.statusCode
    }
}
