package com.equalexperts.products.service.providers

import com.equalexperts.products.domain.Product
import com.equalexperts.products.service.MediaTypes
import com.fasterxml.jackson.databind.ObjectMapper
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap
import spock.lang.Specification

import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import java.lang.annotation.Annotation

class ProductV1MessageBodyWriterSpec extends Specification {
    private ObjectMapper objectMapper

    def setup() {
        objectMapper = new ObjectMapper()
    }

    def "should produce the correct media types"() {
        expect:
        def annotation = ProductV1MessageBodyWriter.getAnnotation(Produces)

        annotation != null

        def value = annotation.value()
        value.size() == 2

        value.contains(MediaType.APPLICATION_JSON)
        value.contains(MediaTypes.PRODUCT_V1_JSON)
    }

    def "should indicate it is the writer for products"() {
        expect:
        def writer = new ProductV1MessageBodyWriter(objectMapper)

        writer.isWriteable(clazz, null, new Annotation[0], new MediaType()) == result

        where:
        clazz   | result
        Product | true
        String  | false
    }

    def "should return the correct representation"() {
        given:
        def product = new Product(productId: "productId", description: "A product", price: 19999, color: "red")
        def writer = new ProductV1MessageBodyWriter(objectMapper)

        def outputStream = new ByteArrayOutputStream()

        when:
        writer.writeTo(product, Product, null, new Annotation[0], new MediaType(), new MultivaluedStringMap(), outputStream)

        then:
        def value = objectMapper.readValue(outputStream.toByteArray(), Map)

        value.productId == product.productId
        value.description == product.description
        value.price == product.price
        value.color == product.color
    }
}
