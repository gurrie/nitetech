package com.equalexperts.products.service.providers

import com.equalexperts.products.domain.Product
import com.equalexperts.products.service.MediaTypes
import com.fasterxml.jackson.databind.ObjectMapper
import org.glassfish.jersey.internal.util.collection.MultivaluedStringMap
import spock.lang.Specification

import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import java.lang.annotation.Annotation

class ProductV2MessageBodyWriterSpec extends Specification {
    private ObjectMapper objectMapper

    def setup() {
        objectMapper = new ObjectMapper()
    }

    def "should produce the correct media types"() {
        expect:
        def annotation = ProductV2MessageBodyWriter.getAnnotation(Produces)

        annotation != null

        def value = annotation.value()
        value.size() == 1

        value.contains(MediaTypes.PRODUCT_V2_JSON)
    }

    def "should indicate it is the writer for products"() {
        expect:
        def writer = new ProductV2MessageBodyWriter(objectMapper)

        writer.isWriteable(clazz, null, new Annotation[0], new MediaType()) == result

        where:
        clazz   | result
        Product | true
        String  | false
    }

    def "should return the correct representation"() {
        given:
        def product = new Product(productId: "productId", description: "A product", price: 19999, color: "red")
        def writer = new ProductV2MessageBodyWriter(objectMapper)

        def outputStream = new ByteArrayOutputStream()

        when:
        writer.writeTo(product, Product, null, new Annotation[0], new MediaType(), new MultivaluedStringMap(), outputStream)

        then:
        def value = objectMapper.readValue(outputStream.toByteArray(), Map)

        value.productId == product.productId
        value.description == product.description
        value.price == "199.99"
        value.color == product.color
    }
}
