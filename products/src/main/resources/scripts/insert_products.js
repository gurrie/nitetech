var products = [
    {
        "productId": "gib-sgj-a1",
        "description": "Gibson SGJ 2014",
        "price": "49999",
        "color": "Cherry Satin"
    },
    {
        "productId": "gib-sgj-a2",
        "description": "Gibson SGJ 2014",
        "price": "49999",
        "color": "Fireburst Satin"
    },
    {
        "productId": "gib-sgsp-a1",
        "description": "Gibson SG Special 2014",
        "price": "69999",
        "color": "Cherry Vintage Gloss"
    },
    {
        "productId": "gib-sgsp-a2",
        "description": "Gibson SG Special 2014",
        "price": "69999",
        "color": "Fireburst Vintage Gloss"
    },
    {
        "productId": "sg-sgst-a1",
        "description": "Gibson SG Standard 120",
        "price": "139999",
        "color": "Cherry"
    },
    {
        "productId": "sg-sgst-a2",
        "description": "Gibson SG Standard 120",
        "price": "139999",
        "color": "Ebony"
    }
];

db.products.remove({});

products.forEach(function(product) {
    db.products.insert(product);
});
