package com.equalexperts.products.domain;

import com.google.common.collect.Lists;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

import javax.inject.Inject;
import java.util.List;

public class ProductRepository {
    private MongoCollection collection;

    @Inject
    public ProductRepository(Jongo jongo) {
        this.collection = jongo.getCollection("products");

        createIndexes();
    }

    public List<Product> list() {
        return Lists.newArrayList(collection.find().as(Product.class).iterator());
    }

    public Product findByProductId(String productId) {
        return collection.findOne("{ productId: \'" + productId + "\' }").as(Product.class);
    }

    private void createIndexes() {
        collection.ensureIndex("{ 'productId': 1 }", "{ unique: 1 }");
    }
}
