package com.equalexperts.products.domain;

import org.jongo.marshall.jackson.oid.ObjectId;

public class Product {
    @ObjectId
    private String _id;
    private String productId;
    private String description;
    private Integer price;
    private String color;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
