package com.equalexperts.products.service;

public class MediaTypes {
    public static final String PRODUCT_V1_JSON = "application/vnd.ee.product.v1+json";
    public static final String PRODUCT_V2_JSON = "application/vnd.ee.product.v2+json";
    public static final String PRODUCT_LIST_V1_JSON = "application/vnd.ee.product-list.v1+json";
    public static final String PRODUCT_LIST_V2_JSON = "application/vnd.ee.product-list.v2+json";
}
