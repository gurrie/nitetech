package com.equalexperts.products.service.providers;

import com.equalexperts.common.providers.AbstractListMessageBodyWriter;
import com.equalexperts.documentation.generator.OutputSchema;
import com.equalexperts.products.domain.Product;
import com.equalexperts.products.service.MediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Provider
@OutputSchema("schemas/product-list-v1-schema.json")
@Produces({MediaType.APPLICATION_JSON, MediaTypes.PRODUCT_LIST_V1_JSON})
public class ProductListV1MessageBodyWriter extends AbstractListMessageBodyWriter<List<Product>> {
    @Inject
    public ProductListV1MessageBodyWriter(ObjectMapper objectMapper) {
        super(objectMapper, new GenericType<List<Product>>() {});
    }

    @Override
    public List<Map<String, Object>> getRepresentation(List<Product> value) {
        return value.stream().map(product -> {
            Map<String, Object> map = new LinkedHashMap<>(4);
            map.put("productId", product.getProductId());
            map.put("description", product.getDescription());
            map.put("price", product.getPrice());
            map.put("color", product.getColor());

            return map;
        }).collect(Collectors.toList());
    }

}
