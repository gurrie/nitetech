package com.equalexperts.products.service.providers;

import com.equalexperts.common.providers.AbstractListMessageBodyWriter;
import com.equalexperts.documentation.generator.OutputSchema;
import com.equalexperts.products.domain.Product;
import com.equalexperts.products.service.MediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.ext.Provider;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

@Provider
@Produces(MediaTypes.PRODUCT_LIST_V2_JSON)
@OutputSchema("schemas/product-list-v2-schema.json")
public class ProductListV2MessageBodyWriter extends AbstractListMessageBodyWriter<List<Product>> {
    @Inject
    public ProductListV2MessageBodyWriter(ObjectMapper objectMapper) {
        super(objectMapper, new GenericType<List<Product>>() {});
    }

    @Override
    public List<Map<String, Object>> getRepresentation(List<Product> value) {
        NumberFormat numberFormatter = new DecimalFormat("#.00");

        return value.stream().map(product -> {
            Map<String, Object> map = new LinkedHashMap<>(4);
            map.put("productId", product.getProductId());
            map.put("description", product.getDescription());
            map.put("price", numberFormatter.format(product.getPrice() / 100.0));
            map.put("color", product.getColor());
            return map;
        }).collect(toList());
    }
}
