package com.equalexperts.products.service.resources;

import com.equalexperts.products.domain.Product;
import com.equalexperts.products.domain.ProductRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

import static com.equalexperts.products.service.MediaTypes.*;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * Product Resource
 */
@Path("/product")
public class ProductResource {
    private ProductRepository productRepository;

    @Inject
    public ProductResource(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    /**
     * Returns a list of all the products in the system.

     * @response.representation.200.doc The request was successful.
     *
     * @return A representation of all the products in the system
     */
    @GET
    @Path("list")
    @Produces({APPLICATION_JSON, PRODUCT_LIST_V1_JSON, PRODUCT_LIST_V2_JSON})
    public List<Product> getProductList() {
        return productRepository.list();
    }

    /**
     * Returns a representation of the product specified by the supplier product id.
     *
     * @param productId The id of the product that the request is for
     *
     * @response.representation.200.doc The request was successful.
     * @response.representation.404.doc The request was not successful. No product exists for the supplied id.

     * @return The representation of the requested product.
     */
    @GET
    @Path("{productId}")
    @Produces({APPLICATION_JSON, PRODUCT_V1_JSON, PRODUCT_V2_JSON})
    public Product getProductByProductId(@PathParam("productId") String productId) {
        Product product = productRepository.findByProductId(productId);

        if (product == null) {
            throw new NotFoundException(String.format("No product found for id %s", productId));
        }

        return product;
    }
}
