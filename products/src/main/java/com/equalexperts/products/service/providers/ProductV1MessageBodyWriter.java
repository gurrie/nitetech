package com.equalexperts.products.service.providers;

import com.equalexperts.common.providers.AbstractMessageBodyWriter;
import com.equalexperts.documentation.generator.OutputSchema;
import com.equalexperts.products.domain.Product;
import com.equalexperts.products.service.MediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.util.LinkedHashMap;
import java.util.Map;

@Provider
@OutputSchema("schemas/product-v1-schema.json")
@Produces({MediaType.APPLICATION_JSON, MediaTypes.PRODUCT_V1_JSON})
public class ProductV1MessageBodyWriter extends AbstractMessageBodyWriter<Product> {
    @Inject
    public ProductV1MessageBodyWriter(ObjectMapper objectMapper) {
        super(objectMapper, Product.class);
    }

    @Override
    public Map<String, Object> getRepresentation(Product value) {
        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("productId", value.getProductId());
        map.put("description", value.getDescription());
        map.put("price", value.getPrice());
        map.put("color", value.getColor());

        return map;
    }

}
