package com.equalexperts.products.service.providers;

import com.equalexperts.common.providers.AbstractMessageBodyWriter;
import com.equalexperts.documentation.generator.OutputSchema;
import com.equalexperts.products.domain.Product;
import com.equalexperts.products.service.MediaTypes;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.Provider;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.Map;

@Provider
@Produces(MediaTypes.PRODUCT_V2_JSON)
@OutputSchema("schemas/product-v2-schema.json")
public class ProductV2MessageBodyWriter extends AbstractMessageBodyWriter<Product> {

    @Inject
    public ProductV2MessageBodyWriter(ObjectMapper objectMapper) {
        super(objectMapper, Product.class);
    }

    @Override
    public Map<String, Object> getRepresentation(Product value) {
        DecimalFormat numberFormatter = new DecimalFormat("#.##");

        Map<String, Object> map = new LinkedHashMap<>(4);
        map.put("productId", value.getProductId());
        map.put("description", value.getDescription());
        map.put("price", numberFormatter.format(value.getPrice() / 100.0));
        map.put("color", value.getColor());

        return map;
    }
}
