package com.equalexperts.products.config;

import com.equalexperts.documentation.generator.DocumentedWadlGenerator;
import com.equalexperts.products.service.providers.ProductListV1MessageBodyWriter;
import com.equalexperts.products.service.providers.ProductListV2MessageBodyWriter;
import com.equalexperts.products.service.providers.ProductV1MessageBodyWriter;
import com.equalexperts.products.service.providers.ProductV2MessageBodyWriter;
import org.glassfish.jersey.server.wadl.config.WadlGeneratorConfig;
import org.glassfish.jersey.server.wadl.config.WadlGeneratorDescription;
import org.glassfish.jersey.server.wadl.internal.generators.WadlGeneratorApplicationDoc;
import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.WadlGeneratorResourceDocSupport;

import javax.ws.rs.ext.MessageBodyWriter;
import java.util.ArrayList;
import java.util.List;

public class ProductWadlGeneratorConfig extends WadlGeneratorConfig {
    @Override
    public List<WadlGeneratorDescription> configure() {
        List<Class<? extends MessageBodyWriter>> writers = new ArrayList<>();
        writers.add(ProductV1MessageBodyWriter.class);
        writers.add(ProductV2MessageBodyWriter.class);
        writers.add(ProductListV1MessageBodyWriter.class);
        writers.add(ProductListV2MessageBodyWriter.class);

        return generator(WadlGeneratorApplicationDoc.class).prop("applicationDocsStream", "documentation/applicationDoc.xml").
                generator(WadlGeneratorResourceDocSupport.class).prop("resourceDocStream", "documentation/resourceDoc.xml").
                generator(DocumentedWadlGenerator.class).prop("resourceDocStream", "documentation/resourceDoc.xml")
                                                        .prop("writers", writers)
                .descriptions();
    }
}