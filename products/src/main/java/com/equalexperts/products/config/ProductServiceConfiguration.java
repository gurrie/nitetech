package com.equalexperts.products.config;

import com.equalexperts.common.config.MongoConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

import javax.validation.constraints.NotNull;

public class ProductServiceConfiguration extends Configuration {
    @NotNull
    @JsonProperty
    private MongoConfiguration mongo;

    public MongoConfiguration getMongo() {
        return mongo;
    }
}
