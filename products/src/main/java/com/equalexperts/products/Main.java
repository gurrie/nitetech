package com.equalexperts.products;

import com.equalexperts.products.config.ProductServiceConfiguration;
import io.dropwizard.Application;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.server.ServerProperties;

public class Main extends Application<ProductServiceConfiguration> {
    public static void main(String[] args) throws Exception {
        new Main().run(args);
    }

    @Override
    public void run(ProductServiceConfiguration configuration, final Environment environment) throws Exception {
        JerseyEnvironment jersey = environment.jersey();

        enableDocumentation(jersey);
        configureResourceConfig(configuration, jersey);
    }

    private void enableDocumentation(JerseyEnvironment jersey) {
        jersey.disable(ServerProperties.WADL_FEATURE_DISABLE);
        jersey.property(ServerProperties.WADL_GENERATOR_CONFIG, "com.equalexperts.products.config.ProductWadlGeneratorConfig");
    }

    private void configureResourceConfig(ProductServiceConfiguration configuration, JerseyEnvironment jersey) {
        jersey.packages("com.equalexperts.products.service");
        jersey.packages("com.equalexperts.documentation");
        jersey.register(new DependencyBinder(configuration.getMongo()));
    }
}
