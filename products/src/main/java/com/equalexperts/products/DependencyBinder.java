package com.equalexperts.products;

import com.equalexperts.common.MongoBinder;
import com.equalexperts.common.config.MongoConfiguration;
import com.equalexperts.products.domain.ProductRepository;

import javax.inject.Singleton;

public class DependencyBinder extends MongoBinder {
    public DependencyBinder(MongoConfiguration mongo) {
        super(mongo);
    }

    @Override
    protected void configure() {
        super.configure();

        bind(ProductRepository.class).to(ProductRepository.class).in(Singleton.class);
    }
}
