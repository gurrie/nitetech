package com.equalexperts.simulations

import io.gatling.core.Predef._ 
import io.gatling.http.Predef._
import scala.concurrent.duration._

class OrderSimulation extends Simulation {
  val httpProtocol = http.disableFollowRedirect
    .disableAutoReferer
    .disableClientSharing
    .connection("keep-alive")

  val order_scenario = scenario("Valid Order Scenario").during(60 minutes) {
    exec(http("list products").get("http://localhost:9080/product/list").header("Accept", "application/vnd.ee.product-list.v1+json").check(status.is(200))).pause(1, 3)
    exec(http("view sg").get("http://localhost:9080/product/sg-sgst-a1").header("Accept", "application/vnd.ee.product.v1+json").check(status.is(200))).pause(1, 3)

    exec(http("create order").post("http://localhost:9090/order/create").check(status.is(201)).check(header("Location").saveAs("orderLocation"))).pause(1, 3).
      randomSwitch(
        99.0 -> {
          exec(http("get order").get("${orderLocation}").check(status.is(200))).pause(1, 3).
          exec(http("add order item").put("${orderLocation}/add/sg-sgst-a1").check(status.is(204))).pause(Duration(200, MILLISECONDS), Duration(500, MILLISECONDS)).
          exec(http("submit order item").post("${orderLocation}/submit").check(status.is(204))).pause(Duration(200, MILLISECONDS), Duration(500, MILLISECONDS))
        },
        1.0 -> {
          exec(http("get order").get("${orderLocation}").check(status.is(200))).pause(1).pause(Duration(200, MILLISECONDS), Duration(500, MILLISECONDS))
          exec(http("submit order item").post("${orderLocation}/submit").check(status.is(409))).pause(Duration(200, MILLISECONDS), Duration(500, MILLISECONDS))
        }
      )
  }

  setUp(
    order_scenario.inject(rampUsers(40).over(5 minutes)).protocols(httpProtocol)
  ).assertions(global.failedRequests.count.is(0))
}
