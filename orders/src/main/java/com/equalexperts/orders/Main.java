package com.equalexperts.orders;

import com.codahale.metrics.MetricRegistry;
import com.equalexperts.orders.config.OrderServiceConfiguration;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Environment;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.client.Client;

public class Main extends Application<OrderServiceConfiguration> {
    public static void main(String[] args) throws Exception {
        new Main().run(args);
    }

    @Override
    public void run(OrderServiceConfiguration configuration, final Environment environment) throws Exception {
        Client client = new JerseyClientBuilder(environment).using(configuration.getHttpClient()).build(getName());
        JerseyEnvironment jersey = environment.jersey();

        enableDocumentation(jersey);
        configureResourceConfig(configuration, jersey, client, environment.metrics());
    }

    private void enableDocumentation(JerseyEnvironment jersey) {
        jersey.disable(ServerProperties.WADL_FEATURE_DISABLE);
        jersey.property(ServerProperties.WADL_GENERATOR_CONFIG, "com.equalexperts.orders.config.OrderWadlGeneratorConfig");
    }

    private void configureResourceConfig(OrderServiceConfiguration configuration, JerseyEnvironment jersey, Client client, MetricRegistry metricRegistry) {
        jersey.packages("com.equalexperts.orders.service");
        jersey.packages("com.equalexperts.documentation");
        jersey.register(new DependencyBinder(configuration.getMongo(), configuration.getProductServiceUrl(), client, metricRegistry));
    }
}
