package com.equalexperts.orders;

import com.codahale.metrics.MetricRegistry;
import com.equalexperts.common.MongoBinder;
import com.equalexperts.common.config.MongoConfiguration;
import com.equalexperts.orders.domain.OrderRepository;
import com.equalexperts.orders.service.client.ProductServiceClient;

import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

public class DependencyBinder extends MongoBinder {
    private String productServiceUrl;
    private Client productClient;
    private MetricRegistry metricRegistry;

    public DependencyBinder(MongoConfiguration mongo, String productServiceUrl, Client productClient, MetricRegistry metricRegistry) {
        super(mongo);

        this.productServiceUrl = productServiceUrl;
        this.productClient = productClient;
        this.metricRegistry = metricRegistry;
    }

    @Override
    protected void configure() {
        super.configure();

        bind(productClient.target(productServiceUrl)).to(WebTarget.class);
        bind(metricRegistry).to(MetricRegistry.class);

        bind(OrderRepository.class).to(OrderRepository.class).in(Singleton.class);
        bind(ProductServiceClient.class).to(ProductServiceClient.class).in(Singleton.class);
    }
}
