package com.equalexperts.orders.domain;

public class OrderItem {
    private String productId;
    private int quantity;
    private int price;

    private OrderItem() {}

    public OrderItem(String productId, int price) {
        this.productId = productId;
        this.quantity = 1;
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getPrice() {
        return price;
    }
}
