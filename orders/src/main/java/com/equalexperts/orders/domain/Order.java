package com.equalexperts.orders.domain;

import org.jongo.marshall.jackson.oid.ObjectId;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class Order {
    @ObjectId
    private String _id;
    private String status = "New";

    private Set<OrderItem> orderItems = new LinkedHashSet<>();

    public String getId() {
        return _id;
    }

    public Set<OrderItem> getOrderItems() {
        return Collections.unmodifiableSet(orderItems);
    }

    public String getStatus() {
        return status;
    }

    public void submit() {
        this.status = "Submitted";
    }

    public void addOrderItem(OrderItem orderItem) {
        orderItems.add(orderItem);
    }
}
