package com.equalexperts.orders.domain;

import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.jongo.Oid;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;

@Service
public class OrderRepository {
    private MongoCollection collection;

    @Inject
    public OrderRepository(Jongo jongo) {
        this.collection = jongo.getCollection("orders");
    }

    public Order createOrder() {
        Order order = new Order();
        collection.insert(order);

        return order;
    }

    public Order getOrder(String orderId) {
        return collection.findOne(Oid.withOid(orderId)).as(Order.class);
    }

    public void save(Order order) {
        collection.update(Oid.withOid(order.getId())).with(order);
    }
}
