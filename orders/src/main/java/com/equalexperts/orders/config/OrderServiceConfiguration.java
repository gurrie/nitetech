package com.equalexperts.orders.config;

import com.equalexperts.common.config.MongoConfiguration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.client.JerseyClientConfiguration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class OrderServiceConfiguration extends Configuration {
    @Valid
    @NotNull
    @JsonProperty
    private MongoConfiguration mongo;

    @Valid
    @NotNull
    @JsonProperty
    private JerseyClientConfiguration httpClient = new JerseyClientConfiguration();

    @NotNull
    String productServiceUrl;

    public MongoConfiguration getMongo() {
        return mongo;
    }

    public JerseyClientConfiguration getHttpClient() {
        return httpClient;
    }

    public String getProductServiceUrl() {
        return productServiceUrl;
    }
}
