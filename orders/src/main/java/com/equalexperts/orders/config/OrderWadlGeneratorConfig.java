package com.equalexperts.orders.config;

import com.equalexperts.documentation.generator.DocumentedWadlGenerator;
import com.equalexperts.orders.service.providers.OrderV1MessageBodyWriter;
import org.glassfish.jersey.server.wadl.config.WadlGeneratorConfig;
import org.glassfish.jersey.server.wadl.config.WadlGeneratorDescription;
import org.glassfish.jersey.server.wadl.internal.generators.WadlGeneratorApplicationDoc;
import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.WadlGeneratorResourceDocSupport;

import javax.ws.rs.ext.MessageBodyWriter;
import java.util.ArrayList;
import java.util.List;

public class OrderWadlGeneratorConfig extends WadlGeneratorConfig {
    @Override
    public List<WadlGeneratorDescription> configure() {
        List<Class<? extends MessageBodyWriter>> writers = new ArrayList<>();
        writers.add(OrderV1MessageBodyWriter.class);

        return generator(WadlGeneratorApplicationDoc.class).prop("applicationDocsStream", "documentation/applicationDoc.xml").
                generator(WadlGeneratorResourceDocSupport.class).prop("resourceDocStream", "documentation/resourceDoc.xml").
                generator(DocumentedWadlGenerator.class).prop("resourceDocStream", "documentation/resourceDoc.xml")
                                                        .prop("writers", writers)
                .descriptions();
    }
}