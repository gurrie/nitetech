package com.equalexperts.orders.service.providers;

import com.equalexperts.common.providers.AbstractMessageBodyWriter;
import com.equalexperts.documentation.generator.OutputSchema;
import com.equalexperts.orders.domain.Order;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.equalexperts.orders.service.MediaTypes.ORDER_V1_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Provider
@OutputSchema("schemas/order-v1-schema.json")
@Produces({APPLICATION_JSON, ORDER_V1_JSON})
public class OrderV1MessageBodyWriter extends AbstractMessageBodyWriter<Order> {
    @Inject
    public OrderV1MessageBodyWriter(ObjectMapper objectMapper) {
        super(objectMapper, Order.class);
    }

    @Override
    public Map<String, Object> getRepresentation(Order value) {
        Map<String, Object> representation = new LinkedHashMap<>(2);
        representation.put("id", value.getId());
        representation.put("orderItems", new ArrayList());

        return representation;
    }

}
