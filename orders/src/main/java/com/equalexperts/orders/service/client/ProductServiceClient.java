package com.equalexperts.orders.service.client;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.equalexperts.orders.service.ClientMediaTypes;
import com.equalexperts.orders.service.client.representation.Product;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.client.WebTarget;

public class ProductServiceClient {
    private WebTarget productTarget;
    private Timer requestTimer;

    @Inject
    public ProductServiceClient(WebTarget productTarget, MetricRegistry metricRegistry) {
        this.productTarget = productTarget.path("{productId}");
        this.requestTimer = metricRegistry.timer(MetricRegistry.name(ProductServiceClient.class, "request"));
    }

    public Product findProduct(String productId) {
        Timer.Context time = requestTimer.time();
        try {

            return productTarget.resolveTemplate("productId", productId)
                    .request(ClientMediaTypes.PRODUCT_JSON)
                    .get(Product.class);
        } catch (WebApplicationException e) {
            e.printStackTrace();

            return null;
        } finally {
            time.stop();
        }
    }
}
