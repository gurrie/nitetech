package com.equalexperts.orders.service.commands;

import com.equalexperts.orders.service.client.ProductServiceClient;
import com.equalexperts.orders.service.client.representation.Product;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

import javax.inject.Inject;

public class ProductLookupCommand extends HystrixCommand<Product> {
    private ProductServiceClient productServiceClient;
    private String productId;

    @Inject
    public ProductLookupCommand(ProductServiceClient productServiceClient, String productId) {
        super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("productLookup")).andCommandPropertiesDefaults(HystrixCommandProperties.Setter().withExecutionIsolationThreadTimeoutInMilliseconds(20000)));

        this.productServiceClient = productServiceClient;
        this.productId = productId;
    }

    @Override
    protected Product run() throws Exception {
//        Thread.sleep(10000);

        return productServiceClient.findProduct(productId);
    }
}
