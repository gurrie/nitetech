package com.equalexperts.orders.service.resources;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.equalexperts.orders.domain.Order;
import com.equalexperts.orders.domain.OrderItem;
import com.equalexperts.orders.domain.OrderRepository;
import com.equalexperts.orders.service.client.ProductServiceClient;
import com.equalexperts.orders.service.client.representation.Product;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.equalexperts.orders.service.MediaTypes.ORDER_V1_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.CONFLICT;

/**
 * Order Resource
 */
@Path("/order")
@Produces(APPLICATION_JSON)
public class OrderResource {
    private OrderRepository orderRepository;
    private ProductServiceClient productServiceClient;

    private Counter createdOrdersCounter;
    private Meter createOrdersMeter;
    private Meter failedOrdersMeter;

    @Inject
    public OrderResource(OrderRepository orderRepository, ProductServiceClient productServiceClient, MetricRegistry metricRegistry) {
        this.orderRepository = orderRepository;
        this.productServiceClient = productServiceClient;
        this.createdOrdersCounter = metricRegistry.counter(MetricRegistry.name(OrderResource.class, "total-orders"));
        this.createOrdersMeter = metricRegistry.meter(MetricRegistry.name(OrderResource.class, "created-orders"));
        this.failedOrdersMeter = metricRegistry.meter(MetricRegistry.name(OrderResource.class, "failed-orders"));
    }

    /**
     * Creates a new order and returns a link to the order.
     *
     * @response.representation.201.doc The request was successful, consult the Location header for the location of the order.
     *
     * @return A response indicating that the request was successful.
     */
    @POST
    @Path("create")
    public Response createOrder(@Context UriInfo uriInfo) {
        Order order = orderRepository.createOrder();

        return Response.created(uriInfo.getBaseUriBuilder().path(OrderResource.class).path("{orderId}").build(order.getId())).build();
    }

    /**
     * Returns a representation of the order for the specified order id.
     *
     * @param orderId the id of the order you wish to retrieve
     *
     * @response.representation.200.doc The request was successful.
     * @response.representation.204.doc No order exists for the specified id.
     *
     * @return A response indicating that the request was successful.
     */
    @GET
    @Path("{orderId}")
    @Produces({APPLICATION_JSON, ORDER_V1_JSON})
    public Order getOrder(@PathParam("orderId") String orderId) {
        return lookupOrder(orderId);
    }

    /**
     * Creates a new order item for the product specified by the supplied product id.
     *
     * @param orderId the id of the order you wish to add the new order item to.
     * @param productId the product id of the product the new order item is for.
     *
     * @response.representation.204.doc The request was successful and the new order item was added.
     * @response.representation.400.doc An invalid product id was supplied.
     * @response.representation.404.doc No order exists for the specified id.
     */
    @PUT
    @Path("{orderId}/add/{productId}")
    public void addOrderItem(@PathParam("orderId") String orderId, @PathParam("productId") String productId) {
        Order order = lookupOrder(orderId);
        Product product = productServiceClient.findProduct(productId);

        if (product == null) {
            throw new BadRequestException(String.format("No product found for id %s", productId));
        }

        order.addOrderItem(new OrderItem(product.getProductId(), product.getPrice()));

        orderRepository.save(order);
    }

    /**
     * Submits the order for fulfillment.
     *
     * @param orderId the id of the order you wish to submit
     *
     * @response.representation.204.doc The request was successful and the order has been submitted.
     * @response.representation.404.doc No order exists for the specified id.
     * @response.representation.409.doc The request failed. The order is in an invalid state
     */
    @POST
    @Path("{orderId}/submit")
    public void addOrderItem(@PathParam("orderId") String orderId) throws WebApplicationException {
        Order order = lookupOrder(orderId);

        if (order.getStatus().equals("Submitted")) {
            failedOrdersMeter.mark();
            throw new WebApplicationException("Order has already been submitted", CONFLICT);
        }

        if (order.getOrderItems().isEmpty()) {
            failedOrdersMeter.mark();
            throw new WebApplicationException("Order has no items", CONFLICT);
        }

        order.submit();

        try {
            orderRepository.save(order);
        } catch(Exception e) {
            failedOrdersMeter.mark();
            throw e;
        } finally {
            createOrdersMeter.mark();
            createdOrdersCounter.inc();
        }
    }

    private Order lookupOrder(String orderId) {
        Order order = orderRepository.getOrder(orderId);

        if (order == null) {
            throw new NotFoundException("No order with id 'orderId' exists");
        }
        return order;
    }
}
