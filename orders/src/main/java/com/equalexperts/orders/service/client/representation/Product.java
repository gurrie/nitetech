package com.equalexperts.orders.service.client.representation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {
    @JsonProperty
    private String productId;

    @JsonProperty
    private int price;

    public String getProductId() {
        return productId;
    }

    public int getPrice() {
        return price;
    }
}
