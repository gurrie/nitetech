package com.equalexperts.orders.service.resources

import com.codahale.metrics.MetricRegistry
import com.equalexperts.common.config.MongoConfiguration
import com.equalexperts.orders.DependencyBinder
import com.equalexperts.orders.domain.Order
import com.equalexperts.orders.domain.OrderItem
import com.equalexperts.orders.service.MediaTypes
import com.equalexperts.test.annotations.Resource
import com.equalexperts.test.specification.ResourceSpecification
import com.github.fge.jackson.JsonLoader
import org.bson.types.ObjectId
import org.glassfish.hk2.utilities.binding.AbstractBinder

import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType

import static javax.ws.rs.client.ClientBuilder.newClient
import static javax.ws.rs.core.Response.Status.*

@Resource(
        type = OrderResource,
        collection = "orders",
        providerPkgs = "com.equalexperts.orders.service.providers"
)
class OrderResourceSpec extends ResourceSpecification {
    @Override
    AbstractBinder getBinder() {
        def mongoConfiguration = new MongoConfiguration(host: "app-monitor", port: 27017, db: "orders", user: "order-user", password: "s3cr3t")

        new DependencyBinder(mongoConfiguration, "http://localhost:9999", newClient(), new MetricRegistry())
    }

    def "should create a new order and return the link to it"() {
        given:
        def request = target.path("order/create").request()

        when:
        def response = request.post(Entity.json(null))

        then:
        response.status == CREATED.statusCode

        def orderLocation = response.getHeaderString("Location")
        orderLocation ==~ /http:\/\/localhost:9998\/order\/[a-fA-F0-9]{24}/

        collection.count() == 1
    }

    def "should return an order with the supplied id if it exists"() {
        given:
        def order = new Order()
        collection.insert(order)
def request = target.path("order/{orderId}").resolveTemplate("orderId", order.id).request(mediaType)

        when:
        def response = request.get()

        then:
        response.status == OK.statusCode

        def entity = response.readEntity(String)
        def schema = schemaFactory.getJsonSchema(JsonLoader.fromResource(schemaPath))
        def result = schema.validate(JsonLoader.fromString(entity))

        result.success

        where:
        mediaType                  | schemaPath
        MediaType.APPLICATION_JSON | "/schemas/order-v1-schema.json"
        MediaTypes.ORDER_V1_JSON   | "/schemas/order-v1-schema.json"
    }

    def "should return a 404 if no order exists for the specified id"() {
        given:
        def request = target.path("order/{orderId}").resolveTemplate("orderId", new ObjectId()).request(MediaType.APPLICATION_JSON)

        when:
        def response = request.get()

        then:
        response.status == NOT_FOUND.statusCode
    }

    def "should submit the order for the specified id"() {
        given:
        def order = new Order()
        order.addOrderItem(new OrderItem("1", 19999))

        collection.insert(order)

        def request = target.path("order/{orderId}/submit").resolveTemplate("orderId", order.id).request(MediaType.APPLICATION_JSON)

        when:
        def response = request.post(null)

        then:
        response.status == NO_CONTENT.statusCode

        collection.findOne().as(Order).status == "Submitted"
    }

    def "should return a 404 if no order exists for the specified id on submit"() {
        given:
        def request = target.path("order/{orderId}/submit").resolveTemplate("orderId", new ObjectId()).request(MediaType.APPLICATION_JSON)

        when:
        def response = request.post(null)

        then:
        response.status == NOT_FOUND.statusCode
    }

    def "should return a 409 if an attempt is made to submit an order with no items"() {
        def order = new Order()
        collection.insert(order)

        def request = target.path("order/{orderId}/submit").resolveTemplate("orderId", order.id).request(MediaType.APPLICATION_JSON)

        when:
        def response = request.post(null)

        then:
        response.status == CONFLICT.statusCode

        collection.findOne().as(Order).status == "New"
    }

    def "should return a 409 if an attempt is made to submit an order that has already been submitted"() {
        def order = new Order()
        order.addOrderItem(new OrderItem("1", 19999))
        order.submit()

        collection.insert(order)

        def request = target.path("order/{orderId}/submit").resolveTemplate("orderId", order.id).request(MediaType.APPLICATION_JSON)

        when:
        def response = request.post(null)

        then:
        response.status == CONFLICT.statusCode
    }
}
