package com.equalexperts.orders.service.resources

import com.codahale.metrics.MetricRegistry
import com.equalexperts.common.config.MongoConfiguration
import com.equalexperts.orders.DependencyBinder
import com.equalexperts.orders.domain.Order
import com.equalexperts.test.annotations.Resource
import com.equalexperts.test.specification.ResourceSpecification
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.bson.types.ObjectId
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.jongo.Oid
import org.junit.Rule

import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.Entity
import javax.ws.rs.core.MediaType

import static com.equalexperts.orders.service.ClientMediaTypes.PRODUCT_JSON
import static com.github.tomakehurst.wiremock.client.WireMock.*
import static javax.ws.rs.client.ClientBuilder.newClient
import static javax.ws.rs.core.Response.Status.*

@Resource(
        type = OrderResource,
        collection = "orders",
        providerPkgs = "com.equalexperts.orders.service.providers"
)
class OrderResourceItemSpec extends ResourceSpecification {
    @Rule
    WireMockRule wireMockRule = new WireMockRule(9999)

    Order order

    @Override
    AbstractBinder getBinder() {
        def mongoConfiguration = new MongoConfiguration(host: "app-monitor", port: 27017, db: "orders", user: "order-user", password: "s3cr3t")

        new DependencyBinder(mongoConfiguration, "http://localhost:9999", newClient(), new MetricRegistry())
    }

    def setup() {
        order = new Order()
        collection.insert(order)
    }

    def "should return a 404 if no order exists for the specified id"() {
        given:
        def request = target.path("order/{orderId}/add/1").resolveTemplate("orderId", new ObjectId()).request(MediaType.APPLICATION_JSON)

        when:
        def response = request.put(Entity.entity("", PRODUCT_JSON))

        then:
        assert response.status == NOT_FOUND.statusCode
    }

    def "should look up the product specified by the supplied id on add"() {
        given:
        stubResponse()

        def request = target.path("order/{orderId}/add/1").resolveTemplate("orderId", order.id).request()

        when:
        request.put(Entity.entity("", PRODUCT_JSON))

        then:
        verify(1, getRequestedFor(urlEqualTo("/1")).withHeader("Accept", equalTo(PRODUCT_JSON)))
    }

    def "should add a new order item for the supplied product"() {
        given:
        stubResponse()

        def request = target.path("order/{orderId}/add/1").resolveTemplate("orderId", this.order.id).request()

        when:
        def response = request.put(Entity.entity("", PRODUCT_JSON))

        then:
        response.status == NO_CONTENT.statusCode

        def amendedOrder = collection.findOne(Oid.withOid(order.id)).as(Order)
        amendedOrder.orderItems.size() == 1

        def orderItem = amendedOrder.orderItems.first()
        orderItem.productId == "product1"
        orderItem.price == 19999
        orderItem.quantity == 1
    }

    def "should return an error if no product is found for the supplied product id"() {
        given:
        def request = target.path("order/{orderId}/add/2").resolveTemplate("orderId", this.order.id).request()

        when:
        def response = request.put(Entity.entity("", PRODUCT_JSON))

        then:
        response.status == BAD_REQUEST.statusCode
    }

    def stubResponse() {
        def productResponse = getClass().getResource("/responses/product-detail-response.json").text
        def response = aResponse().withStatus(OK.statusCode).withHeader("Content-Type", PRODUCT_JSON).withBody(productResponse)

        stubFor(get(urlEqualTo("/1")).withHeader("Accept", equalTo(PRODUCT_JSON)).willReturn(response))
    }
}
