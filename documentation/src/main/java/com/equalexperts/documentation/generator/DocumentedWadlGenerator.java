package com.equalexperts.documentation.generator;


import com.sun.research.ws.wadl.*;
import org.glassfish.jersey.server.model.Parameter;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.server.model.ResourceMethod;
import org.glassfish.jersey.server.wadl.WadlGenerator;
import org.glassfish.jersey.server.wadl.internal.ApplicationDescription;
import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.ResourceDocAccessor;
import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.model.MethodDocType;
import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.model.ParamDocType;
import org.glassfish.jersey.server.wadl.internal.generators.resourcedoc.model.ResourceDocType;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.*;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toSet;

public class DocumentedWadlGenerator implements WadlGenerator {
    private Map<String, Class<MessageBodyWriter>> vendorWriters = new HashMap<>();
    private WadlGenerator delegate;
    private InputStream resourceDocStream;
    private ResourceDocAccessor resourceDoc;

    @Override
    public void init() throws Exception {
        delegate.init();

        Unmarshaller unmarshaller = JAXBContext.newInstance(ResourceDocType.class).createUnmarshaller();
        ResourceDocType resourceDocObj = unmarshaller.unmarshal(new StreamSource(resourceDocStream), ResourceDocType.class).getValue();
        resourceDoc = new ResourceDocAccessor(resourceDocObj);
    }

    @Override
    public Request createRequest(Resource resource, ResourceMethod method) {
        Request request = delegate.createRequest(resource, method);

        List<Parameter> params = method.getInvocable().getParameters();
        Optional<Parameter> entity = params.stream().filter(param -> param.getSource() == Parameter.Source.UNKNOWN).findAny();

        if (entity.isPresent()) {
            request.getRepresentation().add(createEntity(method, entity.get()));
        }

        return request;
    }

    @Override
    public List<Response> createResponses(Resource resource, ResourceMethod method) {
        List<Response> responses = delegate.createResponses(resource, method);

        for (Response response : responses) {
            if (response.getStatus().isEmpty() || response.getStatus().contains(200L)) {
                List<Representation> successRepresentations = createSuccessRepresentations(method);

                consolidateRepresentations(response, successRepresentations);
                response.getRepresentation().addAll(successRepresentations);
            }
        }

        return responses;
    }

    private Representation createEntity(ResourceMethod method, Parameter entity) {
        Representation representation = new Representation();

        if (!method.getConsumedTypes().isEmpty()) {
            representation.setMediaType(method.getConsumedTypes().get(0).toString());
        }

        Method resourceMethod = method.getInvocable().getDefinitionMethod();
        Class<?> resourceClass = resourceMethod.getDeclaringClass();

        ParamDocType paramDoc = resourceDoc.getParamDoc(resourceClass, resourceMethod, entity);

        if (paramDoc != null) {
            Doc doc = new Doc();
            doc.getContent().add(paramDoc.getCommentText());
            representation.getDoc().add(doc);
        } else {
            String name = entity.getSourceName();
            final MethodDocType methodDoc = resourceDoc.getMethodDoc(resourceClass, resourceMethod);
            if (methodDoc != null) {
                methodDoc.getParamDocs().stream().filter(paramDocType -> paramDocType.getParamName().equals(name)).forEach(paramDocType -> {
                    Doc doc = new Doc();
                    doc.getContent().add(paramDocType.getCommentText());
                    representation.getDoc().add(doc);
                });
            }
        }

        return representation;
    }

    private List<Representation> createSuccessRepresentations(ResourceMethod method) {
        List<Representation> successRepresentations = new ArrayList<>();

        Set<String> mediaTypes = method.getProducedTypes().stream().map(MediaType::toString).collect(toSet());
        Set<Class<MessageBodyWriter>> writers = mediaTypes.stream().filter(vendorWriters::containsKey).map(vendorWriters::get).collect(toSet());

        for (String mediaType : mediaTypes) {
            Representation representation = new Representation();
            representation.setMediaType(mediaType);
            representation.setId(lookupSchema(mediaType, writers));

            successRepresentations.add(representation);
        }

        return successRepresentations;
    }

    private void consolidateRepresentations(Response response, List<Representation> successRepresentations) {
        List<Representation> representations = response.getRepresentation();

        if (!representations.isEmpty() && !representations.get(0).getDoc().isEmpty()) {
            successRepresentations.forEach(successRepresentation -> successRepresentation.getDoc().addAll(representations.get(0).getDoc()));
        }

        representations.clear();
    }

    private String lookupSchema(String mediaType, Set<Class<MessageBodyWriter>> writers) {
        Optional<Class<MessageBodyWriter>> mediaTypeWriter = writers.stream().filter(writer -> mediaTypesFrom(writer).contains(mediaType)).findAny();
        return schemaFor(mediaTypeWriter);
    }

    public void setWriters(Collection<Class<MessageBodyWriter>> writers) {
        writers.forEach(writer -> {
            Set<String> vendorMediaTypes = mediaTypesFrom(writer).stream().filter(mediaType -> mediaType.matches(".*/vnd.*")).collect(toSet());
            vendorMediaTypes.forEach(vendorMediaType -> vendorWriters.put(vendorMediaType, writer));
        });
    }

    private static List<String> mediaTypesFrom(Class<MessageBodyWriter> writer) {
        return asList(writer.getAnnotation(Produces.class).value());
    }

    private static String schemaFor(Optional<Class<MessageBodyWriter>> writer) {
        if (writer.isPresent() && writer.get().isAnnotationPresent(OutputSchema.class)) {
            return writer.get().getAnnotation(OutputSchema.class).value();
        }

        return null;
    }

    @Override
    public String getRequiredJaxbContextPath() {
        return delegate.getRequiredJaxbContextPath();
    }

    @Override
    public Application createApplication() {
        return delegate.createApplication();
    }

    @Override
    public Resources createResources() {
        return delegate.createResources();
    }

    @Override
    public com.sun.research.ws.wadl.Resource createResource(Resource r, String path) {
        return delegate.createResource(r, path);
    }

    @Override
    public com.sun.research.ws.wadl.Method createMethod(Resource r, ResourceMethod m) {
        return delegate.createMethod(r, m);
    }

    @Override
    public Representation createRequestRepresentation(Resource r, ResourceMethod m, MediaType mediaType) {
        return delegate.createRequestRepresentation(r, m, mediaType);
    }

    @Override
    public Param createParam(Resource r, ResourceMethod m, Parameter p) {
        return delegate.createParam(r, m, p);
    }

    @Override
    public ExternalGrammarDefinition createExternalGrammar() {
        return delegate.createExternalGrammar();
    }

    @Override
    public void attachTypes(ApplicationDescription description) {
        delegate.attachTypes(description);
    }

    @Override
    public void setWadlGeneratorDelegate(WadlGenerator delegate) {
        this.delegate = delegate;
    }

    public void setResourceDocStream(InputStream resourceDocStream) {
        this.resourceDocStream = resourceDocStream;
    }
}