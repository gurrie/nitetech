package com.equalexperts.documentation.resources;

import com.sun.research.ws.wadl.Application;
import jersey.repackaged.com.google.common.io.ByteStreams;
import org.glassfish.jersey.server.wadl.WadlApplicationContext;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;

/**
 * Documentation
 */
@Path("documentation")
public class DocumentationResource {
    private String wadl;
    private String html;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public Response getDocumentationAsHTML(@Context UriInfo uriInfo, @Context WadlApplicationContext wadlApplicationContext) {
        try {
            String html = getWadlHtml(wadlApplicationContext, uriInfo);

            return Response.ok(html).build();
        } catch (Exception e) {
            throw new ServerErrorException("Unable to generate service documentation", Response.Status.INTERNAL_SERVER_ERROR, e);
        }
    }

    @GET
    @Produces(MediaType.TEXT_XML)
    public Response getDocumentationAsXML(@Context UriInfo uriInfo, @Context WadlApplicationContext wadlApplicationContext) {
        try {
            String wadl = getWadlXml(wadlApplicationContext, uriInfo);

            return Response.ok(wadl).build();
        } catch (Exception e) {
            throw new ServerErrorException("Unable to generate service documentation", Response.Status.INTERNAL_SERVER_ERROR, e);
        }
    }

    @GET
    @Path("/schema")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSchema(@QueryParam("name") String schemaFile) {
        InputStream is = getClass().getResourceAsStream("/" + schemaFile);

        if (is ==  null){
            return Response.status(Response.Status.NOT_FOUND).build();
        }

        return Response.ok(is).build();
    }

    private synchronized String getWadlHtml(WadlApplicationContext wadlApplicationContext, UriInfo uriInfo) throws TransformerException, JAXBException {
        if (html == null) {
            html = generateHtml(wadlApplicationContext, uriInfo);
        }

        return html;
    }

    private synchronized String getWadlXml(WadlApplicationContext wadlApplicationContext, UriInfo uriInfo) throws JAXBException {
        if (wadl == null) {
            wadl = generateWadl(wadlApplicationContext, uriInfo);
        }

        return wadl;
    }

    private String generateHtml(WadlApplicationContext wadlApplicationContext, UriInfo uriInfo) throws TransformerException, JAXBException {
        StringWriter writer = new StringWriter();
        String wadl = getWadlXml(wadlApplicationContext, uriInfo);

        StreamSource xmlSource = new StreamSource(new StringReader(wadl));
        StreamSource styleSheetSource = new StreamSource(getClass().getResourceAsStream("/wadl_html_transform.xsl"));

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer handler = transformerFactory.newTransformer(styleSheetSource);

        handler.transform(xmlSource, new StreamResult(writer));

        return writer.toString();
    }

    private String generateWadl(WadlApplicationContext wadlApplicationContext, UriInfo uriInfo) throws JAXBException {
        StringWriter writer = new StringWriter();
        Application application = wadlApplicationContext.getApplication(uriInfo, false).getApplication();

        Marshaller marshaller = wadlApplicationContext.getJAXBContext().createMarshaller();
        marshaller.marshal(application, new StreamResult(writer));

        return writer.toString();
    }
}
