package com.equalexperts.common

import com.equalexperts.common.config.MongoConfiguration
import com.fasterxml.jackson.databind.ObjectMapper
import com.mongodb.DB
import com.mongodb.MongoClient
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.jongo.Jongo

class MongoBinder extends AbstractBinder {
    private MongoConfiguration mongo

    MongoBinder(MongoConfiguration mongo) {
        this.mongo = mongo
    }

    @Override
    protected void configure() {
        bind(new Jongo(getDB())).to(Jongo)
        bind(new ObjectMapper()).to(ObjectMapper)
    }

    private DB getDB() {
        def address = new ServerAddress(mongo.host, mongo.port)
        def credentials = MongoCredential.createMongoCRCredential(mongo.user, mongo.db, mongo.password.toCharArray())
        def client = new MongoClient(address, [credentials])

        client.getDB(mongo.db)
    }
}
