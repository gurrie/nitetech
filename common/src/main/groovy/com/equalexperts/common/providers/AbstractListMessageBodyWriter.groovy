package com.equalexperts.common.providers

import com.fasterxml.jackson.databind.ObjectMapper

import javax.ws.rs.WebApplicationException
import javax.ws.rs.core.GenericType
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.ext.MessageBodyWriter
import java.lang.annotation.Annotation
import java.lang.reflect.Type

abstract class AbstractListMessageBodyWriter<T> implements MessageBodyWriter<T> {
    private ObjectMapper objectMapper
    private GenericType expectedType

    AbstractListMessageBodyWriter(ObjectMapper objectMapper, GenericType expectedType) {
        this.objectMapper = objectMapper
        this.expectedType = expectedType
    }

    @Override
    final long getSize(T t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        -1
    }

    @Override
    final boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        genericType == this.expectedType.type
    }

    @Override
    final void writeTo(T t, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        def representation = getRepresentation(t)

        objectMapper.writeValue(entityStream, representation)
    }

    abstract List<Map<String, Object>> getRepresentation(T value)
}
