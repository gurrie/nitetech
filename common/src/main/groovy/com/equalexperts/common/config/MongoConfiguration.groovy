package com.equalexperts.common.config

import org.hibernate.validator.constraints.NotEmpty

import javax.validation.constraints.NotNull

class MongoConfiguration {
    @NotEmpty
    String host

    @NotNull
    Integer port

    @NotEmpty
    String user

    @NotEmpty
    String password

    @NotEmpty
    String db
}
