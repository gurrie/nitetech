node default {
  import 'puppet/manifests/graphite.pp'
  import 'puppet/manifests/mongo.pp'

  mongodb_user { "order-user":
    ensure        => present,
    password_hash => mongodb_password('order-user', 's3cr3t'),
    database      => orders,
    roles         => ['readWrite', 'dbAdmin'],
    tries         => 10,
    require       => Class['mongodb::server'],
  }

  mongodb_user { "product-user":
    ensure        => present,
    password_hash => mongodb_password('product-user', 's3cr3t'),
    database      => products,
    roles         => ['readWrite', 'dbAdmin'],
    tries         => 10,
    require       => Class['mongodb::server'],
  }

  class { '::collectd':
    purge        => true,
    recurse      => true,
    purge_config => true,
  }

  class { 'collectd::plugin::cpu':
  }

  class { 'collectd::plugin::load':
  }

  class { 'collectd::plugin::df':
    mountpoints    => ['/u'],
    fstypes        => ['nfs','tmpfs','autofs','gpfs','proc','devpts'],
    ignoreselected => true,
  }

  class { 'collectd::plugin::write_graphite':
    graphitehost => 'localhost',
  }

  class { 'collectd::plugin::logfile':
    log_level => 'warning',
    log_file => '/var/log/collected.log'
  }

  include collectd
  include collectd::plugin::cpu
  include collectd::plugin::load
  include collectd::plugin::df
  include collectd::plugin::logfile
  include collectd::plugin::write_graphite
}
